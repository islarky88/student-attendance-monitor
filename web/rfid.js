'use strict';

let room = 'ECE Room 421';
let customTime = '16:21:10'; // comment out this line if you want to set a custom tim
// let customDate = '2020-01-29'; // comment out this line if you want to set a custom time
const enableRfidSimulation = true; // change to false to disable simulation
const isAdminMode = false; // set this to true if you only want to use the admin panel
const sendSmsToGuardian = false; // set this to false so no actial SMS will be sent

const comPort = 'COM7';
const minsToCheckStudentAbsent = 1; // how many mins after teacher logout to check for students cutting classes or absent. this is also the time window students can logout after teacher.
const minsAdvancedLogin = 0; // set this to 0 so users can login start from subjec time start.
const minsAfterSubjectTimeStartToBeAbsent = 20; // if you don't login within this time limit, you will be considered absent.
const minsLateAllowance = 10; // if you login after the minsLateAllowance, you will be considered late. more than that is considered absent
const enableSendSmsToGuardian = true;
const tapProtectionTimeout = 5; //secs for tap protection. also protect against tap in tap out right away

// required modules
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

const io = require('./socket');
const { Op, Log, User, Subject, Class, Attendance } = require('./models.js');
const { format, isAfter, isBefore, addMinutes } = require('date-fns');
const sequelize = require('sequelize');
const mysql = require('mysql2');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'isyotnet_rfid',
});

const arduino = new SerialPort(comPort, { baudRate: 9600 });
// console.log(port);
const parser = new Readline();
arduino.pipe(parser);

// start of reading COM port
console.log('Initializing Arduino RFID Server...');

/* +++++++++++++++++++++++++++++++++++++++++++++++++ */
/* +++++++++++++++++ FUNCTIONS +++++++++++++++++++++ */
/* +++++++++++++++++++++++++++++++++++++++++++++++++ */

const getTime = timestamp => {
  if (timestamp) {
    return timestamp;
  } else if (typeof customTime != 'undefined') {
    return customTime;
  } else {
    return format(Date.now(), 'kk:mm:ss');
  }
};

const sanitize = string => {
  return string.replace(/[^a-zA-Z0-9:-= ]/g, '').trim();
};

const getHour = timestamp => {
  if (timestamp) {
    return format(timestamp, 'kk:mm:ss');
  } else if (typeof customTime != 'undefined') {
    return customTime;
  } else {
    return format(Date.now(), 'kk:mm:ss');
  }
};

const mysqlDateTime = timestamp => {
  if (timestamp) {
    return format(Date.now(), 'yyyy-MM-dd') + ' ' + timestamp;
  } else if (typeof customTime != 'undefined') {
    return format(Date.now(), 'yyyy-MM-dd') + ' ' + customTime;
  } else {
    return format(Date.now(), 'yyyy-MM-dd kk:mm:ss');
  }
};

const mysqlDateOnly = timestamp => {
  if (timestamp) {
    return format(timestamp, 'yyyy-MM-dd');
  } else if (typeof customDate != 'undefined') {
    return customDate;
  } else {
    return format(Date.now(), 'yyyy-MM-dd');
  }
};

const getSubjTimeInMinutes = hourMinute => {
  let split, hrs, mins;
  if (hourMinute) {
    split = hourMinute.split(':');
    hrs = parseInt(split[0]);
    mins = parseInt(split[1]);
  } else if (typeof customTime != 'undefined') {
    split = customTime.split(':');
    hrs = parseInt(split[0]);
    mins = parseInt(split[1]);
  } else {
    split = format(Date.now(), 'kk:mm').split(':');
    hrs = parseInt(split[0]);
    mins = parseInt(split[1]);
  }
  let final = 0;
  final += hrs * 60;
  final += mins;
  return final;
};

const getSubjTimeInSecs = hourMinSec => {
  let split, hrs, mins, secs;
  if (hourMinSec) {
    split = hourMinSec.split(':');
    hrs = parseInt(split[0]);
    mins = parseInt(split[1]);
    secs = parseInt(split[2]);
  } else if (typeof customTime != 'undefined') {
    split = customTime.split(':');
    hrs = parseInt(split[0]);
    mins = parseInt(split[1]);
    secs = parseInt(split[2]);
  } else {
    split = format(Date.now(), 'kk:mm:ss').split(':');
    hrs = parseInt(split[0]);
    mins = parseInt(split[1]);
    secs = parseInt(split[2]);
  }
  let final = 0;
  final += hrs * 3600;
  final += mins * 60;
  final += secs;
  console.log(final);
  return final;
};

/* +++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++ Logic ++++++++++++++++++++++ */
/* +++++++++++++++++++++++++++++++++++++++++++++++++ */

// logic to check the minsAfterSubjectTimeStartToBeAbsent so system can set absent to those who cannot login at specified mins.

// minsAfterSubjectTimeStartToBeAbsent
setInterval(async () => {
  let subjects = await Subject.findAll({
    where: {
      room: room,
    },
    raw: true,
  });
  //  console.log(subjects);

  if (subjects.length === 0) {
    console.log('there are no subjects for this room');
    return false;
  }

  let timeInCheck, timeOutCheck, currentTimeCheck;
  const subjectAbsenceCheckIndex = subjects.findIndex(subj => {
    timeInCheck = getSubjTimeInMinutes(subj.timeStart);
    timeOutCheck = getSubjTimeInMinutes(subj.timeEnd);
    currentTimeCheck = getSubjTimeInMinutes();
    if (
      currentTimeCheck + minsAdvancedLogin >= timeInCheck &&
      currentTimeCheck < timeOutCheck
    ) {
      return true;
    }
  });

  if (subjectAbsenceCheckIndex < 0) {
    console.log('no subjects found at this time');
    return false;
  }



  let currentSubjectCheck = subjects[subjectAbsenceCheckIndex];

  // console.log(currentSubjectCheck);

  if (currentSubjectCheck.checkAbsences === mysqlDateOnly()) {

    // console.log('already done checking for absences for this subject.');
    return false;
  }


  timeInCheck = getSubjTimeInMinutes(currentSubjectCheck.timeStart);
  timeOutCheck = getSubjTimeInMinutes(currentSubjectCheck.timeEnd);
  currentTimeCheck = getSubjTimeInMinutes();
  if (currentTimeCheck > timeInCheck + minsAfterSubjectTimeStartToBeAbsent) {
    console.log('set absent to users ');

    let allUsers = await User.findAll({
      where: {
        subjects: {
          [Op.like]: `%${currentSubjectCheck.id}%`,
        },
      },
      raw: true,
    });

    let currentStudentCheck;
    for (let i = 0; i < allUsers.length; i++) {
      currentStudentCheck = await Attendance.findOne({
        where: {
          userId: allUsers[i].id,
          subjectId: currentSubjectCheck.id,
          date: mysqlDateOnly(),
        },
      });

      // if no data found for this student in attandance, he is absent
      if (!currentStudentCheck) {
        sendMessageList.push({
          number: allUsers[i].guardianNumber,
          guardian: allUsers[i].guardianName,
          name: allUsers[i].firstName + ' ' + allUsers[i].lastName,
          subject: currentSubjectCheck.id,
        });

        const attendance = new Attendance({
          userId: allUsers[i].id,
          subjectId: currentSubjectCheck.id,
          subject: currentSubjectCheck.name,
          logInTime: '00:00:00',
          logOutTime: '00:00:00',
          date: mysqlDateOnly(),
          status: 'Absent',
        });
        attendance.save();
      }

      let currSubj = await Subject.findByPk(currentSubjectCheck.id);
      // console.log('done', currSubj);

      currSubj.checkAbsences = mysqlDateOnly();
      console.log('done checking for absences');
      await currSubj.save();
    } // end for loop
  } // end of check if after timeout

  // timeIn = getSubjTimeInMinutes(subj.timeStart);
  // timeOut = getSubjTimeInMinutes(subj.timeEnd);
  // currentTime = getSubjTimeInMinutes(time24hr);
  // if (currentTime + minsAdvancedLogin >= timeIn && currentTime < timeOut) {
  //   return true;
  // }

  return false;

  // find subject that matches the current time to student's schedule
  let timeIn, timeOut, currentTime;
  const foundSubjectIndex = subjects.findIndex(subj => {
    timeIn = getSubjTimeInMinutes(subj.timeStart);
    timeOut = getSubjTimeInMinutes(subj.timeEnd);
    currentTime = getSubjTimeInMinutes(time24hr);
    if (currentTime + minsAdvancedLogin >= timeIn && currentTime < timeOut) {
      return true;
    }
  });

  console.log('checking for subjects now');
  return false;
  console.log('did not go through');

  // checks all students for this subject
  let students = await User.findAll({
    where: {
      userType: 'student',
      subjects: {
        [Op.like]: `%${subjectId}%`,
      },
    },
    raw: true,
  });

  let currentStudent;
  for (let i = 0; i < students.length; i++) {
    currentStudent = await Attendance.findOne({
      where: {
        userId: students[i].id,
        subjectId: subjectId,
        date: mysqlDateOnly(),
      },
    });

    // if no data found for this student in attandance, he is absent
    if (currentStudent) {
      if (currentStudent.logOutTime === '00:00:00') {
        // naay login time pero wala ni logout. ga cut class
        currentStudent.status = 'Cutting Class';
        currentStudent.save();

        sendMessageList.push({
          number: students[i].guardianNumber,
          guardian: students[i].guardianName,
          name: students[i].firstName + ' ' + students[i].lastName,
          subject: subjectName,
        });
      } else {
        // if naay value logintime ug logouttime, present si student
        currentStudent.status = 'Present';
        currentStudent.save();
      }

      // if student is found that logged in but did not log out, he cut classes.
    } else {
      sendMessageList.push({
        number: students[i].guardianNumber,
        guardian: students[i].guardianName,
        name: students[i].firstName + ' ' + students[i].lastName,
        subject: subjectName,
      });

      const attendance = new Attendance({
        userId: students[i].id,
        subjectId: subjectId,
        subject: subjectName,
        logInTime: '00:00:00',
        logOutTime: '00:00:00',
        date: mysqlDateOnly(),
        status: 'Absent',
      });
      attendance.save();
    }
  } // end for loop
}, 15000);

// logic to always check list to send SMS every 10secs
let sendMessageList = [];
setInterval(() => {
  // console.log(sendMessageList);
  if (sendMessageList.length > 0 && enableSendSmsToGuardian) {
    if (sendSmsToGuardian) {
      arduino.write(
        `sms|${sendMessageList[0].number}|Good Day ${sendMessageList[0].guardian}. This is to inform you that ${sendMessageList[0].name} did not attend ${sendMessageList[0].subject} today.`,
      );
    }

    console.log(`sending message to ${sendMessageList[0].number}`);
    sendMessageList.splice(0, 1);
  }
}, 10000);

const checkAbsentStudents = (subjectId, subjectName, subjectRoom) => {
  console.log(
    'checkAbsentStudents called to find students that are absent after subjectId time out or instructor logs out of room',
  );
  setTimeout(async () => {
    console.log(
      `finding all students that are absent for ${subjectName} at ${subjectRoom} `,
    );
    let students = await User.findAll({
      where: {
        userType: 'student',
        subjects: {
          [Op.like]: `%${subjectId}%`,
        },
      },
      raw: true,
    });

    let currentStudent;
    for (let i = 0; i < students.length; i++) {
      currentStudent = await Attendance.findOne({
        where: {
          userId: students[i].id,
          subjectId: subjectId,
          date: mysqlDateOnly(),
        },
      });

      // if no data found for this student in attandance, he is absent
      if (currentStudent) {
        if (currentStudent.logOutTime === '00:00:00') {
          // naay login time pero wala ni logout. ga cut class
          currentStudent.status = 'Cutting Class';
          currentStudent.save();

          sendMessageList.push({
            number: students[i].guardianNumber,
            guardian: students[i].guardianName,
            name: students[i].firstName + ' ' + students[i].lastName,
            subject: subjectName,
          });
        } else {
          // if naay value logintime ug logouttime, present si student
          currentStudent.status = 'Present';
          currentStudent.save();
        }

        // if student is found that logged in but did not log out, he cut classes.
      } else {
        sendMessageList.push({
          number: students[i].guardianNumber,
          guardian: students[i].guardianName,
          name: students[i].firstName + ' ' + students[i].lastName,
          subject: subjectName,
        });

        const attendance = new Attendance({
          userId: students[i].id,
          subjectId: subjectId,
          subject: subjectName,
          logInTime: '00:00:00',
          logOutTime: '00:00:00',
          date: mysqlDateOnly(),
          status: 'Absent',
        });
        attendance.save();
      }
    } // timeout
    console.log(sendMessageList);
  }, 1000 * minsToCheckStudentAbsent * 60);
};

const rfid = async (rfidTag, roomName, time24hr) => {
  console.log(time24hr, mysqlDateTime());
  // if () {
  //   time24hr = time24hr;
  // } else {
  //   console.log("invalid time format for custom time");
  //   time24hr = getTime();
  // }
  console.log(time24hr, mysqlDateTime());
  const log = new Log({
    rfidTag: rfidTag,
    classroom: roomName,
    time: mysqlDateTime(),
  });
  await log.save();

  // find user based on rfidTag
  let user = await User.findOne({
    where: { rfidTag: rfidTag },
  });
  // console.log(user);

  // check if user exists
  if (!user) {
    arduino.write(
      'lcd|User not found.|Please return this|RFID tag to the|IT Department;',
    );
    console.log('User not found for this RFID Tag');
    return false;
  }

  // check if user has classes for this subject in this room
  let tempSubject = JSON.parse(user.subjects || []);

  let subjects = await Subject.findAll({
    where: {
      id: tempSubject,
    },
    raw: true,
  });
  // console.log(subjects);

  // find subject that matches the current time to student's schedule
  let timeIn, timeOut, currentTime;
  const foundSubjectIndex = subjects.findIndex(subj => {
    timeIn = getSubjTimeInMinutes(subj.timeStart);
    timeOut = getSubjTimeInMinutes(subj.timeEnd);
    currentTime = getSubjTimeInMinutes(time24hr);
    if (currentTime + minsAdvancedLogin >= timeIn && currentTime < timeOut) {
      return true;
    }
  });

  // check if there are subjects found for this time for this user
  if (foundSubjectIndex < 0) {
    arduino.write(
      `lcd|${user.lastName} has|no schedule here.|it seems that it is|your break time;`,
    );
    console.log(
      `${user.userType} have no class here now. It seems that it is your break time.`,
    );
    return false;
  }

  // check if found subject for this time is for this room
  if (subjects[foundSubjectIndex].room !== roomName) {
    arduino.write(
      `lcd|Your class is at|${subjects[foundSubjectIndex].room}|at ${subjects[foundSubjectIndex].timeStart}-${subjects[foundSubjectIndex].timeEnd}|Don't be late;`,
    );
    console.log(
      `${user.userType} have no class here now. Your current class is at '${subjects[foundSubjectIndex].room}' for '${subjects[foundSubjectIndex].name}' at ${subjects[foundSubjectIndex].timeStart}-${subjects[foundSubjectIndex].timeEnd}`,
    );
    return false;
  }

  console.log('found subject', subjects[foundSubjectIndex].id);

  let instructor = await User.findOne({
    where: {
      userType: 'instructor',
      subjects: {
        [Op.like]: `%${subjects[foundSubjectIndex].id}%`,
      },
    },
  });

  if (!instructor) {
    console.log('this subject has no instructors');
    return false;
  }

  // check wethere user already has loggedIn
  let loggedIn = await Attendance.findOne({
    where: {
      userId: user.id,
      subjectId: subjects[foundSubjectIndex].id,
      date: mysqlDateOnly(),
    },
  });

  // console.log(loggedIn);

  let instructorLoggedIn = await Attendance.findOne({
    where: {
      userId: instructor.id,
      subjectId: subjects[foundSubjectIndex].id,
      subject: subjects[foundSubjectIndex].name,
      room: subjects[foundSubjectIndex].room,
      date: mysqlDateOnly(),
    },
  });

  // console.log(instructorLoggedIn);

  // sets the logout time for subject
  let currentLogOutTime = subjects[foundSubjectIndex].timeEnd + ':00';

  // changes the logout time  for this subject if instructor leaves early
  if (
    instructorLoggedIn && // check if instructor is logged in
    instructorLoggedIn.logOutTime !== '00:00:00' && // check attendance data if instructor has not logged out
    getSubjTimeInSecs(instructorLoggedIn.logOutTime) <
      getSubjTimeInSecs(subjects[foundSubjectIndex].timeEnd + ':00') // checks if current time is earlier than actual logout time of subject
  ) {
    currentLogOutTime = instructorLoggedIn.logOutTime;
    console.log('instructor is already in the class room');
  }

  if (!loggedIn) {
    // check if user is late
    let isLate = false;
    console.log(
      'check late',
      getSubjTimeInSecs(subjects[foundSubjectIndex].timeEnd + ':00'),
      getSubjTimeInSecs(time24hr),
    );
    if (
      getSubjTimeInSecs(subjects[foundSubjectIndex].timeStart + ':00') +
        minsLateAllowance * 60 <
      getSubjTimeInSecs(time24hr)
    ) {
      isLate = true;
      console.log('user is late');
    }

    // add attendance entry for this user for this room/subject since user has not logged in
    const attendance = new Attendance({
      userId: user.id,
      subjectId: subjects[foundSubjectIndex].id,
      subject: subjects[foundSubjectIndex].name,
      room: subjects[foundSubjectIndex].room,
      isLate: isLate,
      status: 'In Class',
      logInTime: getTime(time24hr),
      date: mysqlDateOnly(),
    });

    await attendance.save();
    arduino.write(
      `lcd|${user.firstName} ${user.lastName}|${
        subjects[foundSubjectIndex].name
      }|${format(Date.now(), 'LLL dd, uuuu (ccc)')}|${format(
        Date.now(),
        'kk:mm:ss',
      )};`,
    );
    console.log(
      `${user.userType} has logged in '${user.firstName} ${user.lastName}' with subject ${subjects[foundSubjectIndex].name} (room ${subjects[foundSubjectIndex].room}). at ${time24hr}`,
    );
    return false;
  } else {
    // check for double tap or tap mistake. 5mins after login before logging out
    // check if
    // if (timestamp() - loggedIn.logInTime > tapProtectionTimeout) {
    if (loggedIn.status === 'Absent') {
      arduino.write(
        `lcd|You are ABSENT|for this class.|Go to your next|class. Good Luck;`,
      );
      console.log(
        `${user.userType} is ABSENT for this class. User not able to logout after teacher.`,
      );
    } else if (loggedIn.logOutTime === '00:00:00') {
      if (user.userType === 'student') {
        if (loggedIn.status === 'Cutting Class') {
          arduino.write(
            `lcd|You are considered|Cutting Classes|for this subject|Pls see teacher;`,
          );
          console.log(
            `${user.userType} has logged out. logout time updated for this subject`,
          );
        } else if (
          getSubjTimeInSecs(time24hr) >= getSubjTimeInSecs(currentLogOutTime)
        ) {
          // naa dri ang pag check if absent si student
          loggedIn.logOutTime = getTime(time24hr);
          loggedIn.status = 'Present';
          await loggedIn.save();
          arduino.write(
            `lcd|You have logged out|of ${subjects[foundSubjectIndex].name}|Go to your next|class. Good Luck;`,
          );
          console.log(
            `${user.userType} has logged out. logout time updated for this subject`,
          );
          return false;
        } else {
          arduino.write(
            `lcd|Class is not done.|Pls wait for your|instructor or until|time sched is done;`,
          );
          console.log(
            `${user.userType} is trying to cut classes. class has not ended yet.`,
          );
          return false;
        }
      } else {
        loggedIn.logOutTime = getTime(time24hr);
        loggedIn.status = 'Present';
        await loggedIn.save();

        checkAbsentStudents(
          subjects[foundSubjectIndex].id,
          subjects[foundSubjectIndex].name,
          subjects[foundSubjectIndex].room,
        );

        arduino.write(
          `lcd|You have logged out|of ${subjects[foundSubjectIndex].name}|Go to your next|class. Good Luck;`,
        );

        console.log(
          `${user.userType} has logged out. logout time updated for this subject`,
        );
        return false;
      }
    } else {
      arduino.write(
        `lcd|You have already|logged out of|${subjects[foundSubjectIndex].name};`,
      );
      console.log(`${user.userType} has already logout of this subject`);
      return false;
    }
    // } else {
    //   // check and protection if ma double tap
    //   arduino.write(
    //     `lcd|TAP OUT NOT RECORDED|Did you tap fast?|Please don't cut|classes. Thanks.;`,
    //   );
    //   console.log(
    //     `time still below ${tapProtectionTimeout} secs. Please try again later;`,
    //   );
    //   return false;
    // }
  }

  // console.log(loggedIn);

  // check if user has not logged in for this subject
  // set as present for the user (student/instructor)
  if (subjects[foundSubjectIndex].present === false) {
    subjects[foundSubjectIndex].present = true;
    arduino.write(
      `lcd:${user.lastName}, ${user.firstName.trim()[0]}:${
        subjects[foundSubjectIndex].name
      } - ${roomName}`,
    );
    console.log(
      `${user.lastName}, ${
        user.firstName.trim()[0]
      } has entered room '${roomName}' for subject '${
        subjects[foundSubjectIndex].name
      }'`,
    );
  } else {
    // if student taps again if they already logged in for the subject, send message.
    if (user.userType === 'student') {
      arduino.write(
        `lcd|You have already|logged in for this|subject. Enjoy.|School is cool.;`,
      );
      console.log(`You have already logged in for this subject in this room`);
      // if instructor taps again if they already logged in for the subject,
      // check for students that are absent and send SMS to guardian.
    } else if (user.userType === 'instructor') {
      // logic for finding all students under the instructor for given subject, time and room
      let sendSmsList = [];
      let students = await User.findAll({
        where: {
          userType: 'student',
          subjects: { [Op.like]: '%' + roomName + '%' },
        },
        raw: true,
      });

      // iterate function to search thru all student matching based on roomName
      students.forEach(student => {
        // parses the subject array of objects
        let subjs = JSON.parse(student.subjects);
        // console.log(subjs);

        const result = subjs.find(subj => {
          if (subj.room === roomName) {
            let timeIn = getSubjTimeInMinutes(subj.timeIn);
            let timeOut = getSubjTimeInMinutes(subj.timeOut) + 15;
            let currentTime = getSubjTimeInMinutes();
            if (currentTime >= timeIn && currentTime < timeOut) {
              return true;
            }
          }
        });

        if (result && result.present === false) {
          sendSmsList.push({
            name: student.lastName,
            number: student.guardianNumber,
          });
        }
      });

      // console.log(sendSmsList);

      arduino.write(
        `lcd|Logging out|Checking for Absent|students and send|SMS to guardian;`,
      );
      arduino.write(`sms|9453311521|9453311521|9453311521;`);
      console.log(
        `Logging out of this subject. Checking students that are absent and sending SMS to guardian.`,
      );
    }
  }

  // user.subjects = JSON.stringify(subjects);

  const results = await user.save();
};

// rfid('D0031A4');

// Action everytime Serial.println() is called.  .replace(/[^a-zA-Z0-9: ]/g, "")
parser.on('data', async rawMsg => {
  let msg = rawMsg.replace(/[^a-zA-Z0-9: ]/g, '').trim();
  console.log('Arduino: ' + msg);

  // if split serial message from arduino to get what action
  let split = msg.split(':');
  let action, info;

  // action and the message/info/rfid/etc
  if (split.length > 1) {
    action = split[0].trim();
    info = split[1].trim();
  }

  if (action === 'rfid') {
    // arduino.write(
    //   `sms|9083797464|This is to inform you that AMBOT NIMO did not attend ECE101.`,
    // );

    io.getIO().emit('server', {
      type: 'rfid',
      info: info,
      classroom: room,
      time: mysqlDateTime(),
    });

    if (!isAdminMode) {
      rfid(info, room, getTime());
    }
  }

  if (action === 'time') {
    console.log('arduino asked for time');
    arduino.write(
      `idle|Please tap your RFID|on to the "Tap Here"| ${format(
        Date.now(),
        'LLL dd, uuuu (ccc)',
      )}|${format(Date.now(), 'kk:mm:ss')}`,
    );
  }

  if (action === 'msg') {
    //logic to to notify node that arduino is ready to send sms again
  }
});

/* +++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++ Server +++++++++++++++++++++ */
/* +++++++++++++++++++++++++++++++++++++++++++++++++ */

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const cors = require('cors');
const app = express();
const fs = require('fs');

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'web/images');
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + '-' + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(cors());

app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single('image'),
);
app.use('/images', express.static(path.join(__dirname, 'web', 'images')));
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json());

app.get('/api/logs', async (req, res) => {
  let logs = await Log.findAll({
    limit: 30,
    order: [['time', 'DESC']],

    raw: true,
  });

  res.json({ logs: logs });
});

app.delete('/api/attendance', async (req, res) => {
  connection.query('truncate attendances;', function(err, results, fields) {
    // console.log(err, results);
    res.json(results);
  });

  // let logs = await Attendance.sync({ force: true })
});

app.post('/api/user/search', async (req, res) => {
  console.log(req.body.query);
  let user = await User.findAll({
    where: {
      [Op.or]: {
        rfidTag: sanitize(req.body.query),
        lastName: {
          [Op.like]: `%${sanitize(req.body.query)}%`,
        },
        firstName: {
          [Op.like]: `%${sanitize(req.body.query)}%`,
        },
        rfidTag: {
          [Op.like]: `%${sanitize(req.body.query)}%`,
        },
        nickName: {
          [Op.like]: `%${sanitize(req.body.query)}%`,
        },
        course: {
          [Op.like]: `%${sanitize(req.body.query)}%`,
        },
        guardianName: {
          [Op.like]: `%${sanitize(req.body.query)}%`,
        },
        guardianNumber: {
          [Op.like]: `%${sanitize(req.body.query)}%`,
        },
        contactNumber: {
          [Op.like]: `%${sanitize(req.body.query)}%`,
        },
      },
    },
    raw: true,
  });

  if (!user) {
    res.json({ msg: 'no' });
  }

  res.json(user);
});

app.post('/api/image', async (req, res) => {
  console.log('body', req.body.userId);
  console.log('image', req.file.filename);

  let filePath = path.join(__dirname, 'images', req.file.filename);

  fs.copyFile(
    filePath,
    path.join(__dirname, 'public', 'images', 'users', req.body.userId + '.jpg'),
    err => {
      if (err) throw err;
      fs.unlink(filePath, err => {
        if (err) throw err;
      });
    },
  );
});

app.post('/api/user/new', async (req, res) => {
  // console.log(req.body.newUser);
  //  console.log(req.file);

  let user = await User.findOne({
    where: {
      rfidTag: req.body.newUser.rfidTag,
    },
    raw: true,
  });
  if (!user) {
    const user = new User(req.body.newUser);
    const result = await user.save();
    // console.log(result.dataValues);

    res.json({ msg: 'success', userId: result.dataValues.id });
  } else {
    res.json({ msg: 'rfid already in use' });
  }
});

app.get('/api/subjects/:id', async (req, res) => {
  let students = await User.findAll({
    where: {
      subjects: {
        [Op.like]: `%${req.params.id}%`,
      },
    },

    raw: true,
  });

  for (let i = 0; i < students.length; i++) {
    let attend = await Attendance.findOne({
      where: {
        userId: students[i].id,
        subjectId: req.params.id,
        date: mysqlDateOnly(),
      },

      raw: true,
    });

    if (attend) {
      if (attend.isLate === 1) {
        students[i].isLate = 'Yes';
      } else {
        students[i].isLate = 'No';
      }

      if (attend.status === 'Absent') {
        students[i].status = 'Absent';
        students[i].isLate = 'Absent';
      } else if (attend.status === 'Cutting Class') {
        students[i].status = 'Cutting Class';
      } else if (attend.status === 'In Class') {
        students[i].status = 'In Class';
      } else if (attend.logOutTime !== '00:00:00') {
        students[i].status = 'Present';
      } else {
        students[i].status = 'Cut Class';
      }
    } else {
      students[i].status = '';
      students[i].isLate = '';
    }
    console.log(attend, students[i].id, req.params.id);
  }

  res.json(students);
});

app.get('/api/subjects', async (req, res) => {
  let subject = await Subject.findAll({
    raw: true,
  });

  res.json(subject);
});

app.post('/api/subject/new', async (req, res) => {
  const subject = new Subject(req.body.newSubject);
  let errors = [];
  let validTimeStart = true;
  let validTimeEnd = true;

  if (subject.timeStart.match(/^([01]\d|2[0-3]):?([0-5]\d)$/) == null) {
    errors.push('time Start invalid format');
    validTimeStart = false;
  }
  if (subject.timeEnd.match(/^([01]\d|2[0-3]):?([0-5]\d)$/) == null) {
    errors.push('Time End invalid format');
    validTimeStart = false;
  }
  if (subject.name.length < 1) {
    errors.push('Subject name is empty');
  }

  if (subject.room.length < 1) {
    errors.push('Room Name is empty');
  }

  if (validTimeStart && validTimeEnd) {
    if (
      getSubjTimeInMinutes(subject.timeStart) >=
      getSubjTimeInMinutes(subject.timeEnd)
    ) {
      errors.push('Time Start should be earlier than Time End.');
    }
  }
  console.log(subject.timeEnd);
  let info = [];
  let newStart = subject.timeStart + ':00';
  let newEnd = subject.timeEnd + ':00';
  let checkSubject = await Subject.findAll({
    attributes: ['id'],
    where: {
      room: subject.room,
      [Op.or]: [
        {
          timeStart: {
            [Op.lt]: newEnd,
          },
          timeEnd: {
            [Op.gt]: newStart,
          },
        },
      ],
    },
    raw: true,
  });

  checkSubject.forEach(id => {
    info.push(id.id);
  });

  console.log(info);

  if (checkSubject.length > 0) {
    errors.push('There is a conflict with your shedule in this room.');
  }

  //

  if (errors.length > 0) {
    res.json({ msg: 'error', errors: errors, info: info });
  } else {
    await subject.save();
    res.json({ msg: 'success' });
  }
});

app.get('/api/user/:rfidTag', async (req, res) => {
  let user = await User.findOne({
    where: {
      rfidTag: req.params.rfidTag,
    },
    raw: true,
  });
  res.json(user);
});

app.put('/api/user/subjects', async (req, res) => {
  let user = await User.findOne({
    where: {
      id: req.body.id,
    },
  });

  if (!user) {
    res.json({ msg: 'user not found' });
    return false;
  }

  user.subjects = JSON.stringify(req.body.subjects);
  await user.save();
  res.json({ msg: 'success' });
});

app.put('/api/user/:rfidTag/rfid', async (req, res) => {
  let user = await User.findOne({
    where: {
      rfidTag: req.body.old,
    },
  });

  let dupe = await User.findOne({
    where: {
      rfidTag: req.body.new,
    },
    raw: true,
  });

  if (!user) {
    res.json({ msg: 'user not found' });
    return false;
  }

  if (dupe) {
    res.json({ msg: 'rfid already being used by someone else' });
    return false;
  }

  user.rfidTag = req.body.new;
  await user.save();
  res.json({ msg: 'success' });
});

app.put('/api/user/:rfidTag', async (req, res) => {
  let user = await User.findOne({
    where: {
      id: req.body.id,
    },
  });

  if (!user) {
    res.json({ msg: 'no' });
  }

  user.rfidTag = req.body.rfidTag;
  user.firstName = req.body.firstName;
  user.middleName = req.body.middleName;
  user.lastName = req.body.lastName;
  user.nickName = req.body.nickName;
  user.userType = req.body.userType;
  user.guardianName = req.body.guardianName;
  user.guardianNumber = req.body.guardianNumber;
  user.course = req.body.course;

  await user.save();
  res.json({ msg: 'success', userId: req.body.id });
});

app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;
  res.status(status).json({ message: message, data: data });
});

User.hasMany(Attendance, { foreignKey: 'userId' });
Attendance.belongsTo(User, { foreignKey: 'userId' });

const server = app.listen(3000, () =>
  console.log(`Admin panel located at http://localhost:3000`),
);
let sock = io.init(server);
sock.on('connection', socket => {
  console.log('Client connected');

  if (enableRfidSimulation) {
    socket.emit('rfidTest', 'yes');
  }

  socket.on('rfidSimulate', msg => {
    console.log(
      `Simulating RFID Tap... RFID Tag: ${msg.id}; Room: ${msg.room}; Time: ${msg.time};`,
    );

    socket.emit('server', {
      type: 'rfid',
      info: msg.id,
      classroom: msg.room,
      time: mysqlDateTime(msg.time),
    });

    rfid(msg.id, msg.room, msg.time);
  });
});
