const { Op, Log, User, Subject, Attendance } = require('../arduino/models.js');
const { format, isAfter, isBefore, addMinutes } = require('date-fns');
const mysql = require('mysql2');

const db = mysql.createConnection({
  host: '143.95.68.248',
  user: 'isyotnet_rfid',
  password: 'isyotnet_rfid',
  database: 'isyotnet_rfid',
});

const express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

const WebSocket = require('ws');

var WebSocketServer = require('ws').Server,
  wss = new WebSocketServer({ port: 8080 });

let sendMsg = false;
let message = '';
wss.on('connection', function(ws) {
  ws.on('message', function(message) {
    console.log('received: %s', message);
  });

  setInterval(() => {
    if (sendMsg === true) {
      ws.send(message);
      sendMsg = false;
    }
  }, 50);
});

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'images');
  },
  filename: (req, file, cb) => {
    cb(null, `${new Date().toISOString()}-${file.originalname}`);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.use(express.static('admin-panel/public'));
// app.use(bodyParser.json()); // application/json
// app.use(multer({ storage: fileStorage, fileFilter }).single('image'));
// app.use(cors());

app.get('/api/logs', async (req, res) => {
  let logs = await Log.findAll({
    limit: 30,
    raw: true,
  });

  res.json({ logs: logs });
});

app.get('/api/user/:rfidTag', async (req, res) => {
  let user = await User.findOne({
    where: {
      rfidTag: req.params.rfidTag,
    },
    raw: true,
  });
  res.json(user);
});

app.put('/api/user/:rfidTag/rfid', async (req, res) => {
  let user = await User.findOne({
    where: {
      rfidTag: req.body.old,
    },
  });

  if (!user) {
    res.json({ msg: 'no' });
  }

  user.rfidTag = req.body.new;
  await user.save();
  res.json({ msg: 'yes' });
});

app.put('/api/user/:rfidTag', async (req, res) => {
  let user = await User.findOne({
    where: {
      id: req.body.id,
    },
  });

  if (!user) {
    res.json({ msg: 'no' });
  }

  user.rfidTag = req.body.rfidTag;
  user.firstName = req.body.firstName;
  user.middleName = req.body.middleName;
  user.lastName = req.body.lastName;
  user.nickName = req.body.nickName;
  user.userType = req.body.userType;
  user.guardianName = req.body.guardianName;
  user.guardianNumber = req.body.guardianNumber;
  user.course = req.body.course;

  await user.save();
  res.json({ msg: 'yes' });
});

app.listen(3000, () =>
  console.log(`Admin panel located at http://localhost:3000`),
);
