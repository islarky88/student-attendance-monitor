const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const sequelize = new Sequelize(
  "isyotnet_rfid", // database name
  "root", // user name
  "", // password
  {
    host: "localhost", // hostname
    dialect: "mysql",
    logging: false,
    timestamps: false
  }
);

const Log = sequelize.define("logs", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  rfidTag: {
    type: Sequelize.STRING
  },
  classroom: {
    type: Sequelize.STRING
  },

});

const Subject = sequelize.define(
  "subjects",
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },
    room: {
      type: Sequelize.STRING
    },
    timeStart: {
      type: Sequelize.STRING
    },
    timeEnd: {
      type: Sequelize.STRING
    },
    checkAbsences: {
      type: Sequelize.DATEONLY
    }
  },
  { timestamps: false }
);

const Attendance = sequelize.define("attendances", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  userId: {
    type: Sequelize.STRING
  },
  subjectId: {
    type: Sequelize.STRING,
    defaultValue: 0
  },
  subject: {
    type: Sequelize.STRING
  },
  room: {
    type: Sequelize.STRING,
    defaultValue: '',
  },
  logInTime: {
    type: Sequelize.TIME
  },
  logOutTime: {
    type: Sequelize.TIME
  },
  date: {
    type: Sequelize.DATEONLY
  },
  isLate: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  status: {
    type: Sequelize.STRING,
    defaultValue: 'No Data',
  }
});


Attendance.trunc = () => {
  console.log(sequelize.QueryTypes.SELECT);
  // 'this' refers directly back to the model (the capital "P" Pug)
  return Attendance.sequelize.query(
    'TRUNCATE attendances;',
    {
    },
  );
};


const Class = sequelize.define("classes", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  subject: {
    type: Sequelize.STRING
  },
  classroom: {
    type: Sequelize.STRING
  },
  timeIn: {
    type: Sequelize.TIME
  },
  timeOut: {
    type: Sequelize.TIME
  }
});

const User = sequelize.define("users", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  rfidTag: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  userId: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  firstName: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  middleName: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  lastName: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  nickName: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  contactNumber: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  email: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  password: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  userType: {
    type: Sequelize.STRING,
    defaultValue: "student"
  },
  guardianName: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  guardianNumber: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  course: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  subjects: {
    type: Sequelize.STRING,
    defaultValue: "[]"
  },
  currentSubject: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  currentActivity: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  lastSubject: {
    type: Sequelize.STRING,
    defaultValue: ""
  }
});

module.exports = { Log, Subject, User, Op, Attendance };
