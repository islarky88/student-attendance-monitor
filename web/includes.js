'use strict';

const { Log, User, Subject } = require('./models.js');
const { format, isAfter, isBefore } = require('date-fns');


// db.query(
//   'INSERT INTO `subjects` (`name`, `room`, `timeStart`, `timeEnd`) VALUES (?, ?, ?, ?)',
//   ['ECE 101', roomName, '12:00', '13:30'],
//   function(err, results) {
//     console.log(results);
//   },
// );

// console.log(msg);

// if (line.match(/msg:/gi)) {
//   console.log('Message received: ' + line.split(':')[1]);
// }
// console.log(`> ${line}`);

// db.query(
//   'INSERT INTO `logs` (`rfidTag`, `classroom`, `time`) VALUES (?, ?, ?)',
//   [line.split(':')[1], 'ECE Bldg room 4', Date.now()],
//   function(err, results) {
//     console.log(results);
//   },
// );

// // sendMsg = true;
// message =
//   line.split(':')[1].split('\r')[0] +
//   ':' +
//   'ECE Bldg room 4' +
//   ':' +
//   Date.now();

// wss.on('connection', function connection(ws) {
//   ws.on('message', function incoming(message) {
//     console.log('received: %s', message);
//   });

//   ws.send(line);
// });
// dataToSend = Math.random().toString();
// port.write(dataToSend);
// port.write("s");

// setInterval(() => {

//   port.write("+639453311520");
// }, 2000);

// app.get('/api/logs', (req, res) => {
//   db.query(
//     'SELECT * FROM `logs` ORDER BY `id` DESC LIMIT 30',
//     (err, results) => {
//       res.json({ logs: results });
//     },
//   );
// });

// app.get('/api/user/:rfidTag', (req, res) => {
//   db.query(
//     'SELECT * FROM `users` WHERE rfidTag = ? LIMIT 1',
//     [req.params.rfidTag],
//     (err, results) => {
//       res.json(results[0]);
//     },
//   );
// });

// app.put('/api/user/:rfidTag/rfid', (req, res) => {
//   // console.log(req);
//   //
//   console.log(req.body);
//   db.query(
//     'UPDATE users SET rfidTag = ? WHERE rfidTag = ? LIMIT 1',
//     [req.body.old, req.body.new],
//     (err, results) => {
//       res.json(results);
//     },
//   );

//   // res.json({ msg: 'yes' });
// });

// app.put('/api/user/:rfidTag', (req, res) => {
//   // console.log(req);
//   //
//   console.log(req.body);
//   db.query(
//     'UPDATE users SET rfidTag = ?, firstName = ?, middleName = ?, lastName = ?, nickName = ?, userType = ?, guardianName = ?, guardianNumber = ?, course = ?  WHERE id = ? LIMIT 1',
//     [
//       req.body.rfidTag,
//       req.body.firstName,
//       req.body.middleName,
//       req.body.lastName,
//       req.body.nickName,
//       req.body.userType,
//       req.body.guardianName,
//       req.body.guardianNumber,
//       req.body.course,
//       req.body.id,
//     ],
//     (err, results) => {
//       res.json(results);
//     },
//   );

//   // res.json({ msg: 'yes' });
// });

// app.listen(3000, () => console.log(`Example app listening on port 3000!`));

module.exports = {
  getCurrentSubjectByTime,
  logRfidTap,
  getHour,
  userTimeIn,
  userTimeOut,
  getUserDataByRfid,
  sendSmsToGuardian,
  dismissClass,
};
