-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 27, 2020 at 05:44 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isyotnet_rfid`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

DROP TABLE IF EXISTS `attendances`;
CREATE TABLE IF NOT EXISTS `attendances` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `subjectId` int(10) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `room` varchar(50) NOT NULL,
  `logInTime` time NOT NULL DEFAULT '00:00:00',
  `logOutTime` time NOT NULL DEFAULT '00:00:00',
  `date` date NOT NULL,
  `isLate` tinyint(1) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'No Data',
  `createdAt` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `updatedAt` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `userId`, `subjectId`, `subject`, `room`, `logInTime`, `logOutTime`, `date`, `isLate`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 1, 10005, 'History 101', '', '00:00:00', '00:00:00', '2020-01-27', 0, 'Absent', '2020-01-27 04:58:59', '2020-01-27 04:58:59'),
(2, 2, 10005, 'History 101', '', '00:00:00', '00:00:00', '2020-01-27', 0, 'Absent', '2020-01-27 04:58:59', '2020-01-27 04:58:59'),
(3, 3, 10005, 'History 101', '', '00:00:00', '00:00:00', '2020-01-27', 0, 'Absent', '2020-01-27 04:58:59', '2020-01-27 04:58:59'),
(4, 4, 10005, 'History 101', '', '00:00:00', '00:00:00', '2020-01-27', 0, 'Absent', '2020-01-27 04:58:59', '2020-01-27 04:58:59');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(45) DEFAULT NULL,
  `classroom` varchar(45) DEFAULT NULL,
  `timeIn` time DEFAULT '00:00:00',
  `timeOut` time DEFAULT '00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rfidTag` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `classroom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `time` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `rfidTag`, `classroom`, `time`, `createdAt`, `updatedAt`) VALUES
(1, 'D0031A4', 'ECE Room 421', '1970-01-01 00:00:00', '2020-01-27 05:14:41', '2020-01-27 05:14:41'),
(2, 'D0031A4', 'ECE Room 421', '1970-01-01 00:00:00', '2020-01-27 05:16:27', '2020-01-27 05:16:27'),
(3, 'D0031A4', 'ECE Room 421', '1970-01-01 00:00:00', '2020-01-27 05:17:16', '2020-01-27 05:17:16'),
(4, 'D0031A4', 'ECE Room 421', '1970-01-01 00:00:00', '2020-01-27 05:18:07', '2020-01-27 05:18:07');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `room` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `timeStart` time NOT NULL,
  `timeEnd` time NOT NULL,
  `checkAbsences` date NOT NULL DEFAULT '1970-01-01',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10012 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `room`, `timeStart`, `timeEnd`, `checkAbsences`) VALUES
(10001, 'ECE 101', 'ECE Room 421', '09:00:00', '10:30:00', '1970-01-01'),
(10002, 'ECE 102', 'ECE Room 421', '10:30:00', '12:00:00', '1970-01-01'),
(10003, 'ECE 103', 'ECE Room 421', '12:00:00', '13:30:00', '2020-01-27'),
(10004, 'English 101', 'ECE Room 421', '13:30:00', '16:00:00', '1970-01-01'),
(10005, 'History 101', 'ECE Room 421', '16:00:00', '17:30:00', '2020-01-27'),
(10006, 'Psych 101', 'ECE Room 421', '17:30:00', '18:30:00', '1970-01-01'),
(10007, 'Test 101', 'ECE Room 421', '18:30:00', '19:00:00', '1970-01-01'),
(10008, 'Math 101', 'ECE Room 421', '19:00:00', '19:30:00', '1970-01-01'),
(10011, 'ECE Test', 'ECE Room 421', '02:00:00', '04:00:00', '1970-01-01'),
(10010, 'asdasd', 'asdasd', '01:22:00', '08:59:00', '1970-01-01'),
(10009, 'Nursing 101', 'Room 1', '09:00:00', '10:30:00', '1970-01-01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rfidTag` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `userId` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `firstName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `middleName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `nickName` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `contactNumber` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `userType` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `guardianName` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `guardianNumber` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `course` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subjects` text COLLATE utf8_unicode_ci NOT NULL,
  `currentSubject` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `currentActivity` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `lastSubject` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `lastActivity` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `createdAt` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `updatedAt` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `rfidTag`, `userId`, `firstName`, `middleName`, `lastName`, `nickName`, `contactNumber`, `email`, `password`, `userType`, `guardianName`, `guardianNumber`, `course`, `subjects`, `currentSubject`, `currentActivity`, `lastSubject`, `lastActivity`, `createdAt`, `updatedAt`) VALUES
(1, 'D0031A4', '544905', 'Ace', 'Brodith', 'Maranga', 'Ace', '09453311520', 'islarky88@gmail.com', 'asdqweasd', 'instructor', 'Joanna Ranido', '9453311521', 'BS ECE -4', '[10001,10003,10005,10007,10010,10009,10008,10006,10004,10002,10011]', '', '', '', '2020-01-12 07:58:03', '0000-00-00 00:00:00', '2020-01-12 07:58:03'),
(2, '90FB824D', '544906', 'Jomarie', 'Ambot', 'Marfil', 'Jomz', '09453311521', 'joanna.ranido@gmail.com', 'asdqweasd', 'student', 'Guardian', '9453311520', 'BS ECE-4', '[10001,10003,10005,10007,10010,10009,10008,10006,10004,10002,10011]', '', '', '', '2020-01-26 07:08:21', '0000-00-00 00:00:00', '2020-01-26 07:06:54'),
(3, 'A0E8C77E', '5449231', 'John Luke', 'Ogobs', 'Ceniza', 'JL', '09083797464', 'yuki_murazaki@yahoo.com', 'asdqweasd', 'student', 'Guardian 2', '9453311521', 'Nursing -4 2', '[10001,10003,10005,10007,10010,10009,10008,10006,10004,10002,10011]', '', '', '', '2020-01-26 07:05:47', '0000-00-00 00:00:00', '2020-01-26 07:04:01'),
(4, 'A0E8C77F', '544905', 'Joanna', 'Empeso', 'Ranido', 'Jana', '09453311520', 'islarky88@gmail.com', 'asdqweasd', 'student', 'Guardian 3', '9083797464', 'BS ECE -4', '[10002,10004,10005,10007,10011,10010]', '', '', '', '2020-01-12 08:54:57', '1000-01-01 00:00:00', '2020-01-12 08:54:57');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
