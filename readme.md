# RFID Student Attandance Monitor w/ SMS Notification

## Install Needed Softwares
```
1. Git >> https://git-scm.com/downloads
2. NodeJS(LTS) >>  https://nodejs.org/en/download/
3. VS Code >> https://code.visualstudio.com/
```

### Clone this repository and install packages
```
1. After installing Git. You will have access to Git "Bash".
2. Go to your desktop (or anywhere you want to copy the project files) and 
    press "right click"
3. Choose "Git Bash Here" and a popup window will run for Bash.
4. type "git clone https://gitlab.com/islarky88/student-attendance-monitor.git" 
    and press enter. It will download the latest version of the project
5. type "cd student-attendance-monitor" and press enter to change to the 
    directory of the project
6. type "npm install" to install needed packages for the project. After this 
    you are ready to run the project
```

### RFID Server Backend
```
1. Connect the RFID Arduino Box and Take note of the COM port. You can find it on 
    the "Device Manager" under "Ports (COM & LPT)" usually named "USB Serial Device (COMX)"
2. Import "database.sql" found on the root of the project folder to your Mysql 
    Database. Take note of your database name, user, pass, hostname.
3. Open "web/rfid.js" and change the "comPort" located at line 3 of the file to 
    your COM port which you took note.
4. Take note of the "roomName" on the rfid.js file on line 4 as you will need 
    this to change the room name depending of the subject and schedule of user.
5. Open "web/models.js" and change the MySQL details on the file located at line 5.
6. On Git Bash on the project folder (or on windows, go to project folder and 
    right click anywhere to open "git Bash Here")
7. type "node web/rfid.js" and press enter to run. press "ctrl + c" to quit
8. Have fun logging in users
```

### To get updates....
```
1. Go to project folder.
2. right click anywhere and choose "Git Bash Here"
3. type "git pull" press enter
4. wait. done.
```

### Access Admin Panel (optional)
```
You can use the access panel to check, edit, register new users and check the logs.

Admin Panel >> http://localhost:3000/

Username: admin
Password: admin
```

### Issues
/*
subjects edit add -- DONE
user add registration -- DONE
sms messaging to fix -- DONE
student goes to cr or something -- ok siguro
class schedule on admin -- done?
check for lates all users timesched based 
change time
teacher absent case but student present

15mins late instructor from timeStart. instructor considered absent. then class ends. no one present or absent



*/
