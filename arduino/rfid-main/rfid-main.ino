int msgDuration = 4000;


#include <SPI.h>     // RC522 Module uses SPI protocol
#include <MFRC522.h> // Library for Mifare RC522 Devices
#include <SoftwareSerial.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>


LiquidCrystal_I2C lcd(0x3F, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display


//Create software serial object to communicate with SIM800L
SoftwareSerial mySerial(12, 13); //SIM800L Tx & Rx is connected to Arduino #3 & #2
int _timeout;
String _buffer;
String smsNumber; //-> change with your number
String smsMessage; //-> change with your number
String currentRfidTag;

String msgFromNode;
String action;

byte readCard[4]; // Stores scanned ID read from RFID Module

byte lcdMsg = 0;


unsigned long timeNow;
unsigned long lastTimeUpdate;
unsigned long lastLcdMsgTime;

#define SS_PIN 53
#define RST_PIN 49
MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance.

///////////////////////////////////////// Setup ///////////////////////////////////
void setup()
{

   

    pinMode(18, OUTPUT);
    digitalWrite(18, LOW);
    pinMode(19, OUTPUT);
    digitalWrite(19, HIGH);

  Serial.begin(9600); // Initialize serial communications with PC
  _buffer.reserve(50);
  mySerial.begin(9600);
  SPI.begin();        // MFRC522 Hardware uses SPI protocol
  mfrc522.PCD_Init(); // Initialize MFRC522 Hardware

  Serial.println("Initializing...");
  //    mySerial.println("Initializing SMS...");
  delay(1000);

  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(2, 0);
  lcd.print("RFID Attendance");
  lcd.setCursor(0, 1);
  lcd.print("w/ SMS notification");
  lcd.setCursor(3, 2);
  lcd.print("to Guardian if ");
  lcd.setCursor(2, 3);
  lcd.print("student is absent");


  //
  //  mySerial.println("AT"); //Once the handshake test is successful, it will back to OK
  //  updateSerial();
  //  mySerial.println("AT+CSQ"); //Signal quality test, value range is 0-31 , 31 is the best
  //  updateSerial();
  //  mySerial.println("AT+CCID"); //Read SIM information to confirm whether the SIM is plugged
  //  updateSerial();
  //  mySerial.println("AT+CREG?"); //Check whether it has registered in the network
  //  updateSerial();
}

///////////////////////////////////////// Main Loop ///////////////////////////////////
void loop()
{
  timeNow = millis();
  // function to always check for RFID tap
  getID();

  if (timeNow - lastTimeUpdate >= 1000) {
    lastTimeUpdate = timeNow;

    //    Serial.println("time:yes");
  }



  // Intervals
  if (timeNow - lastLcdMsgTime >= msgDuration && lcdMsg == 1) {
    lcdMsg = 0;
    lastLcdMsgTime = timeNow;
    lcd.clear();
    lcd.setCursor(2, 0);
    lcd.print("RFID Attendance");
    lcd.setCursor(0, 1);
    lcd.print("w/ SMS notification");
    lcd.setCursor(3, 2);
    lcd.print("to Guardian if ");
    lcd.setCursor(2, 3);
    lcd.print("student is absent");
  }



  if (Serial.available() > 0) {
    msgFromNode = Serial.readStringUntil(';');

    action = splitString(msgFromNode, '|', 0);

    if (action == "lcd") {

      lcdMsg = 1;
      lastLcdMsgTime = timeNow;
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(splitString(msgFromNode, '|', 1));
      lcd.setCursor(0, 1);
      lcd.print(splitString(msgFromNode, '|', 2));
      lcd.setCursor(0, 2);
      lcd.print(splitString(msgFromNode, '|', 3));
      lcd.setCursor(0, 3);
      lcd.print(splitString(msgFromNode, '|', 4));

    } else if (action == "sms") {

      smsNumber = splitString(msgFromNode, '|', 1); // 10 digit number (eg. 9453311520)
      smsMessage = splitString(msgFromNode, '|', 2); // 10 digit number (eg. 9453311520)

      //send SMS message to guardian
      sms();


    } else if (action == "idle") {
      lcdMsg = 1;
      lastLcdMsgTime = timeNow;
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(splitString(msgFromNode, '|', 1));
      lcd.setCursor(0, 1);
      lcd.print(splitString(msgFromNode, '|', 2));
      lcd.setCursor(0, 2);
      lcd.print(splitString(msgFromNode, '|', 3));
      lcd.setCursor(0, 3);
      lcd.print(splitString(msgFromNode, '|', 4));

    }



  }
  //  updateSerial();

  //   sendSms();
  //
  //   delay(5000);

  if (mySerial.available() > 0) {
    Serial.write(mySerial.read());
  }
}

String splitString(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

//void showMessage() {
//  lcd.setCursor(3, 0);
//  lcd.print(msg);
//  lcd.setCursor(2, 1);
//  lcd.print("Ywrobot Arduino!");
//  lcd.setCursor(0, 2);
//  lcd.print("Arduino LCM IIC 2004");
//  lcd.setCursor(2, 3);
//  lcd.print("Power By Ec-yuan!");
//
//}

void sendSms()
{
  mySerial.println("AT"); //Once the handshake test is successful, it will back to OK
  updateSerial();
  mySerial.println("AT+CMGF=1"); // Configuring TEXT mode
  updateSerial();
  mySerial.print("AT+CMGS=\"+63");
  mySerial.print("9453311521");
  mySerial.println("\"");

  updateSerial();
  mySerial.println("RFID tag: " + currentRfidTag); //text content
  updateSerial();
  mySerial.println((char)26);
}
void sms()
{
  mySerial.println("AT"); //Once the handshake test is successful, it will back to OK
  updateSerial();
  mySerial.println("AT+CREG?"); // Configuring TEXT mode
  updateSerial();
  mySerial.println("AT+CSCS=GSM"); // Configuring TEXT mode
  updateSerial();
  mySerial.println("AT+CMGF=1"); // Configuring TEXT mode
  updateSerial();
  mySerial.println("AT+CMGS=\"+63" + smsNumber + "\"");//change ZZ with country code and xxxxxxxxxxx with phone number to sms
  updateSerial();
  mySerial.print(smsMessage);
  updateSerial();
  mySerial.println((char)26);
}

void sendMessage()
{
  //Serial.println ("Sending Message");
  mySerial.println("AT+CMGF=1"); //Sets the GSM Module in Text Mode
  delay(1000);
  //Serial.println ("Set SMS Number");
  mySerial.println("AT+CMGS=\"+63" + smsNumber + "\"\r"); //Mobile phone number to send message
  delay(1000);
  mySerial.println(smsMessage);
  delay(1000);
  mySerial.println((char)26); // ASCII code of CTRL+Z
  delay(1000);
  _buffer = _readSerial();
}

String _readSerial()
{
  _timeout = 0;
  while (!mySerial.available() && _timeout < 12000)
  {
    delay(13);
    _timeout++;
  }
  if (mySerial.available())
  {
    return mySerial.readString();
  }
}

///////////////////////////////////////// Get PICC's UID ///////////////////////////////////
int getID()
{
  // Getting ready for Reading PICCs
  if (!mfrc522.PICC_IsNewCardPresent())
  { //If a new PICC placed to RFID reader continue
    return 0;
  }
  if (!mfrc522.PICC_ReadCardSerial())
  { //Since a PICC placed get Serial and continue
    return 0;
  }
  // There are Mifare PICCs which have 4 byte or 7 byte UID care if you use 7 byte PICC
  // I think we should assume every PICC as they have 4 byte UID
  // Until we support 7 byte PICCs
  Serial.print("rfid:");
  currentRfidTag = "";
  for (int i = 0; i < 4; i++)
  { //

    readCard[i] = mfrc522.uid.uidByte[i];
    currentRfidTag = currentRfidTag + readCard[i];
    Serial.print(readCard[i], HEX);
  }
  Serial.println("");
  //  Serial.println(currentRfidTag);
  mfrc522.PICC_HaltA(); // Stop reading

  //  sendSms();
  return 1;
}

void updateSerial()
{
  delay(500);
//  while (Serial.available())
//  {
//    mySerial.write(Serial.read());//Forward what Serial received to Software Serial Port
//  }
//  while (mySerial.available())
//  {
//    Serial.write(mySerial.read());//Forward what Software Serial received to Serial Port
//  }
}
