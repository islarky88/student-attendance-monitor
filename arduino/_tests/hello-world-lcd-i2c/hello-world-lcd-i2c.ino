//YWROBOT
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F, 20, 4); // set the LCD address to 0x27 for a 16 chars and 2 line display

String message;


void setup()
{

  Serial.begin(9600);
  // initialize the lcd
  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(3, 0);
  lcd.print("Hello, world!");
  lcd.setCursor(2, 1);
  lcd.print("Ywrobot Arduino!");
  lcd.setCursor(0, 2);
  lcd.print("Arduino LCM IIC 2004");
  lcd.setCursor(2, 3);
  lcd.print("Power By Ec-yuan!");
}


String stringSplit(String data, char separator, int index) {
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;
  String finalData = "f";
  char currentChar;

  for (int i = 0; i <= maxIndex; i++) {
    currentChar = data.charAt(i);

    Serial.print(currentChar);
    Serial.print(" - ");
    Serial.print(separator);
    if (currentChar == separator) {

      found++;
      Serial.println(found);
    } else {
      if (found == index) {
        finalData = finalData + "a";
      }
    }


  }
  return finalData;

  //  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

String stringSplit2(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
//    return "asd";
}

void loop()
{
  message = "ace:bee:cat:dog";
  message = message + ":egg";
  Serial.print("msg: ");
  Serial.println(stringSplit2(message, ':', 5));
  Serial.print("char: ");
  Serial.println(message.length());
  delay(1000);
}
